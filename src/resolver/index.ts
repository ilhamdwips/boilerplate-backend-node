import * as fs from "fs";

export default function initializeResolver() {
    const modules = (fs.readdirSync(__dirname))
		.filter((it) => !it.endsWith(".map") && !it.startsWith("index."))
        .map((it) => it.replace(".ts", "").replace(".js", ""));

    const resolver = modules.map((module) => {
        return require(`${__dirname}/${module}`).default;
    });

    return resolver;
}
