import {module} from "../decorator/module";
import {get, post} from "../decorator/route";
import * as path from "path";
import * as fs from "fs";
import {ApplicationError} from "../helper/error_handler";
import * as uuid from "uuid";
import * as send from "koa-send";
import {appConfig} from "../config/app";

@module("/files")
export default class FileModule {
    @post("/", [])
    public async get(ctx) {

        if (!ctx.request.files || !ctx.request.files.file) {
            throw new ApplicationError({
                message: "No File Detected",
                type: "NoFileDetectedError",
                detail: {}
            });
        }
        const {request: {body}} = ctx;

        const files = ctx.request.files.file;

        let extension = path.extname(files.name);

        const fileId = uuid.v4();
        const newFilename = fileId + extension;
        const newFilenamePath = path.join(appConfig.upload_dir, newFilename);

        const apiFilePath = `/files/${newFilename}`;

        await fs.promises.rename(files.path, newFilenamePath);

        ctx.status = 201;
        ctx.body = {
            message: "Success Upload",
            path: apiFilePath
        };
    }

    @get("/:id", [])
    public async getFile(ctx) {
        const sendResult = await send(ctx, ctx.params.id, {
            root: appConfig.upload_dir
        });
    }
}
